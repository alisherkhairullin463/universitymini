package Subjects;

public class History implements Subject {
    @Override
    public int getCreditNumber() {
        return 2;
    }

    @Override
    public String getNameOfCourse() {
        return "history";
    }
}
