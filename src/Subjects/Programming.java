package Subjects;


public class Programming implements Subject {
    @Override
    public int getCreditNumber() {
        return 5;
    }

    @Override
    public String getNameOfCourse() {
        return "programming";
    }
}
