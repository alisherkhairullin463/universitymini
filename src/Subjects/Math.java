package Subjects;

public class Math implements Subject{
    public Math() {
    }

    @Override
    public int getCreditNumber() {
        return 5;
    }

    @Override
    public String getNameOfCourse() {
        return "math";
    }
}
