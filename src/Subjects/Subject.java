package Subjects;

public interface Subject {
    int getCreditNumber();
    String getNameOfCourse();
}
