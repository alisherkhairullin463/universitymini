import Persons.Lecturer;
import Persons.Student;
import Subjects.Subject;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Lecturer> lecturers;
    private List<Student> students;
    private List<Subject> subjects;

    public University() {
        lecturers = new ArrayList<>();
        subjects = new ArrayList<>();
        students = new ArrayList<>();
    }

    public List<Lecturer> getLecturers() {
        return lecturers;
    }

    public void setLecturers(List<Lecturer> lecturers) {
        this.lecturers = lecturers;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
    public void addStudentList(List<Student> studentLst){
        students.addAll(studentLst);
    }
    public void addLecturer(Lecturer lecturer) {
        lecturers.add(lecturer);
    }
    public void addSubject(Subject subject){
        subjects.add(subject);
    }
}
