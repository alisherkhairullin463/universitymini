import Persons.Lecturer;
import Persons.Student;
import Subjects.Math;
import Subjects.Programming;
import Subjects.Subject;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        University university = new University();
        Subject math = new Math();
        Subject programming = new Programming();

        university.addSubject(math);
        university.addSubject(programming);

        Student student = new Student(1, "Jake", "Jhonson", 19, new ArrayList<>(List.of(math, programming)));
        Student student1 = new Student(2, "Fin", "Mertens", 18, new ArrayList<>(List.of(math, programming)));
        Student student2 = new Student(3, "Bass", "Breakman", 21, new ArrayList<>(List.of(math, programming)));

        List<Student> students = new ArrayList<>(List.of(student, student1, student2));
        Lecturer lecturer = new Lecturer(4, "Marcy", "Vumbert", 35, 20000.0, math);

        university.addStudentList(students);
        university.addLecturer(lecturer);

        for (Student st : university.getStudents()) {
            lecturer.getObserverManager().addObserver(st);
        }

        university.addLecturer(lecturer);
        lecturer.getObserverManager().notifyObservers();
    }
}