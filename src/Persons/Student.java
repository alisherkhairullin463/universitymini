package Persons;

import Subjects.Subject;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person implements Observer{
    private List<Subject> subjects;

    public Student(int id, String name, String surname, int age, List<Subject> subjects) {
        super(id, name, surname, age);
        this.subjects = subjects;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public void handleMessage(String message) {
        System.out.println("Student " + getName() + " received update: " + message);
    }
}
