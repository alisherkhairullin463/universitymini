package Persons;

public interface Observer {
    void handleMessage(String message);
}
