package Persons;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ObserverManager implements Observed{
    private Set<Observer> observers = new HashSet<>();

    public Set<Observer> getObservers() {
        return observers;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        System.out.println("Write some message for students");
        String message = new Scanner(System.in).nextLine();
        for(Observer observer : observers){
            observer.handleMessage(message);
        }
    }
}
