package Persons;

import Subjects.Subject;

import java.util.List;
import java.util.Scanner;

public class Lecturer extends Person{
    private ObserverManager observerManager;
    private double salary;
    private Subject subject;

    public Lecturer(int id, String name, String surname, int age, double salary, Subject subject) {
        super(id, name, surname, age);
        this.salary = salary;
        this.subject = subject;
        this.observerManager = new ObserverManager();
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;

    }

    public ObserverManager getObserverManager() {
        return observerManager;
    }

    public void setObserverManager(ObserverManager observerManager) {
        this.observerManager = observerManager;
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "id="+getId()+
                "name="+getName()+
                "surname="+getSurname()+
                "age="+getAge()+
                "salary=" + salary +
                ", subject=" + subject +
                ", students=" + observerManager.getObservers() +
                '}';
    }
}
